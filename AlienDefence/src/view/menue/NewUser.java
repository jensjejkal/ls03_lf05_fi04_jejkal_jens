package view.menue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import org.eclipse.wb.swing.FocusTraversalOnArray;

import model.User;
import model.persistanceDB.UserDB;

import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JButton;

public class NewUser extends JFrame {

	private JPanel contentPane;
	private JTextField txtLoginname_1;
	private JTextField txtVorname_1;
	private JTextField txtNachname_1;
	private JTextField txtGeburtstag;
	private JTextField txtStrae;
	private JTextField txtHausnummer;
	private JTextField txtStadt;
	private JTextField txtGehaltserwartungen;
	private JTextField txtFamilienstand;
	private JTextField txtAbschlussnote;
	private JPasswordField pwdPasswort;
	private String loginname;
	private String passwort;
	private String vorname;
	private String nachname;
	private String geburtstag;
	private String strasse;
	private String hausnummer;
	private String stadt;
	private String gehaltserwartungen;
	private String familienstand;
	private String abschlussnote;
	private JTextField txtPostleitzahl;
	private String postleitzahl;

	/**
	 * Create the frame.
	 */
	public NewUser() {
		setVisible(true);
		setMinimumSize(new Dimension(600, 450));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLoginname = new JLabel("Loginname");
		lblLoginname.setBounds(7, 5, 72, 55);
		lblLoginname.setPreferredSize(new Dimension(200, 16));
		contentPane.add(lblLoginname);
		
		txtLoginname_1 = new JTextField();
		txtLoginname_1.setBounds(91, 13, 98, 47);
		txtLoginname_1.setPreferredSize(new Dimension(200, 22));
		txtLoginname_1.setText("Loginname");
		contentPane.add(txtLoginname_1);
		txtLoginname_1.setColumns(10);
		
		JLabel lblPasswort = new JLabel("Passwort");
		lblPasswort.setBounds(7, 73, 72, 47);
		contentPane.add(lblPasswort);
		
		pwdPasswort = new JPasswordField();
		pwdPasswort.setBounds(91, 73, 98, 42);
		pwdPasswort.setText("Passwort");
		contentPane.add(pwdPasswort);
		
		JLabel lblVorname = new JLabel("Vorname");
		lblVorname.setBounds(7, 106, 76, 62);
		contentPane.add(lblVorname);
		
		txtVorname_1 = new JTextField();
		txtVorname_1.setBounds(91, 128, 98, 31);
		txtVorname_1.setText("Vorname");
		contentPane.add(txtVorname_1);
		txtVorname_1.setColumns(10);
		
		JLabel lblNachname = new JLabel("Nachname");
		lblNachname.setBounds(7, 170, 76, 43);
		contentPane.add(lblNachname);
		
		txtNachname_1 = new JTextField();
		txtNachname_1.setBounds(91, 167, 94, 43);
		txtNachname_1.setText("Nachname");
		contentPane.add(txtNachname_1);
		txtNachname_1.setColumns(10);
		
		JLabel lblGeburtstag = new JLabel("Geburtstag");
		lblGeburtstag.setBounds(7, 216, 76, 52);
		contentPane.add(lblGeburtstag);
		
		txtGeburtstag = new JTextField();
		txtGeburtstag.setBounds(91, 224, 98, 37);
		txtGeburtstag.setText("Geburtstag");
		contentPane.add(txtGeburtstag);
		txtGeburtstag.setColumns(10);
		
		JLabel lblStrae = new JLabel("Strasse");
		lblStrae.setBounds(7, 273, 72, 42);
		contentPane.add(lblStrae);
		
		txtStrae = new JTextField();
		txtStrae.setBounds(91, 273, 98, 42);
		txtStrae.setText("Strasse");
		contentPane.add(txtStrae);
		txtStrae.setColumns(10);
		
		JLabel lblHausnummer = new JLabel("Hausnummer");
		lblHausnummer.setBounds(235, 5, 76, 55);
		contentPane.add(lblHausnummer);
		
		
		txtHausnummer = new JTextField();
		txtHausnummer.setBounds(323, 17, 110, 31);
		txtHausnummer.setText("Hausnummer");
		contentPane.add(txtHausnummer);
		txtHausnummer.setColumns(10);
		
		JLabel lblStadt = new JLabel("Stadt");
		lblStadt.setBounds(235, 73, 76, 31);
		contentPane.add(lblStadt);
		
		txtStadt = new JTextField();
		txtStadt.setBounds(323, 70, 110, 34);
		txtStadt.setText("Stadt");
		contentPane.add(txtStadt);
		txtStadt.setColumns(10);
		
		JLabel lblGehaltserwartungen = new JLabel("Gehaltserwartungen");
		lblGehaltserwartungen.setBounds(235, 176, 78, 31);
		contentPane.add(lblGehaltserwartungen);
		
		txtGehaltserwartungen = new JTextField();
		txtGehaltserwartungen.setBounds(323, 176, 135, 31);
		txtGehaltserwartungen.setText("Gehaltserwartungen");
		contentPane.add(txtGehaltserwartungen);
		txtGehaltserwartungen.setColumns(10);
		
		JLabel lblFamilienstand = new JLabel("Familienstand");
		lblFamilienstand.setBounds(235, 227, 98, 31);
		contentPane.add(lblFamilienstand);
		
		txtFamilienstand = new JTextField();
		txtFamilienstand.setBounds(323, 227, 110, 31);
		txtFamilienstand.setText("Familienstand");
		contentPane.add(txtFamilienstand);
		txtFamilienstand.setColumns(10);
		
		JLabel lblAbschlussnote = new JLabel("Abschlussnote");
		lblAbschlussnote.setBounds(235, 273, 87, 43);
		contentPane.add(lblAbschlussnote);
		
		txtAbschlussnote = new JTextField();
		txtAbschlussnote.setBounds(323, 273, 110, 42);
		txtAbschlussnote.setText("Abschlussnote");
		contentPane.add(txtAbschlussnote);
		txtAbschlussnote.setColumns(10);
		
		JButton btnBesttigen = new JButton("Best\u00E4tigen");
		btnBesttigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int p_user_id = -1;
				loginname = txtLoginname_1.getText();
				passwort = pwdPasswort.getText();
				vorname = txtVorname_1.getText();
				nachname = txtNachname_1.getText();
				geburtstag = txtGeburtstag.getText();
				strasse = txtStrae.getText();
				stadt = txtStadt.getText();
				postleitzahl = txtPostleitzahl.getText();
				hausnummer = txtHausnummer.getText();
				gehaltserwartungen = txtGehaltserwartungen.getText();
				familienstand = txtFamilienstand.getText();
				abschlussnote = txtAbschlussnote.getText();
				
				LocalDate geburtstagL = LocalDate.parse(geburtstag);
				int gehaltserwartungenI = Integer.parseInt(gehaltserwartungen);
				double abschlussnoteD = Double.parseDouble(abschlussnote);
				User user = new User(p_user_id, vorname, nachname, geburtstagL, strasse, hausnummer, postleitzahl, stadt, loginname, passwort, gehaltserwartungenI, familienstand, abschlussnoteD);
				//System.out.println(user);
				
				//UserDB.createUser(user);
				setVisible(false);
			}
		});
		btnBesttigen.setBounds(148, 365, 97, 25);
		contentPane.add(btnBesttigen);
		
		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		btnBeenden.setBounds(257, 365, 97, 25);
		contentPane.add(btnBeenden);
		
		JLabel lblPostleitzahl = new JLabel("Postleitzahl");
		lblPostleitzahl.setBounds(235, 125, 88, 25);
		contentPane.add(lblPostleitzahl);
		
		txtPostleitzahl = new JTextField();
		txtPostleitzahl.setText("Postleitzahl");
		txtPostleitzahl.setBounds(323, 126, 121, 31);
		contentPane.add(txtPostleitzahl);
		txtPostleitzahl.setColumns(10);
		
		
		

	}
}
		
		
		

