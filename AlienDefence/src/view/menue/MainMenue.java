package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.GameController;
import model.Level;
import model.persistanceDB.PersistanceDB;
import model.User;
import view.menue.Highscore;
import view.game.GameGUI;
import java.awt.Frame;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.CardLayout;

public class MainMenue extends JFrame {

	private static final long serialVersionUID = 1L;
	private AlienDefenceController alienDefenceController;
	private JPanel contentPane;
	private static JTextField tfdLogin;
	private static JPasswordField pfdPassword;
	@SuppressWarnings("rawtypes")
	private static JComboBox cboLevelChooser;
	private List<Level> arrLevel;
	int type_id;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MainMenue(AlienDefenceController alienDefenceController) {
		setUndecorated(true);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));

		this.alienDefenceController = alienDefenceController;

		// Allgemeine JFrame-Einstellungen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 195, 518);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int x = screenSize.width;
		int y = screenSize.height;
		setBounds(0,0, x, y);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		BorderLayout bl_contentPane = new BorderLayout();
		bl_contentPane.setVgap(50);
		contentPane.setLayout(bl_contentPane);
		setContentPane(contentPane);
		setResizable(false);
		
	    
		// �berschrift
		JLabel lblHeadline = new JLabel("ALIEN DEFENCE");
		lblHeadline.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeadline.setForeground(new Color(124, 252, 0));
		lblHeadline.setFont(new Font("OCR A Extended", Font.BOLD, 50));
		contentPane.add(lblHeadline, BorderLayout.NORTH);

		// Alles unter dem Bild, Tabellenlayout mit einer Spalte
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		contentPane.add(pnlButtons, BorderLayout.SOUTH);
		pnlButtons.setLayout(new GridLayout(0, 1, 0, 5));
		
		JButton btnNewUser = new JButton("Neuer User erstellen");
		pnlButtons.add(btnNewUser);
		btnNewUser.setFont(new Font("Microsoft JhengHei", Font.BOLD, 20));
		btnNewUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new NewUser();
			}
		});

		// Nutzername
		//JLabel lblLogin = new JLabel("Login:");
		//panel.add(lblLogin);
		//lblLogin.setFont(new Font("Cambria Math", Font.BOLD, 18));
		//lblLogin.setForeground(Color.orange);
				
						//tfdLogin = new JTextField();
						//tfdLogin.setPreferredSize(new Dimension(120, 22));
						//panel.add(tfdLogin);
						//tfdLogin.setColumns(10);
		
				// Passwort
				//JLabel lblPassword = new JLabel("Passwort:");
				//panel.add(lblPassword);
				//lblPassword.setFont(new Font("Cambria Math", Font.BOLD, 18));
				//lblPassword.setForeground(Color.orange);
		
				//pfdPassword = new JPasswordField();
				//pfdPassword.setPreferredSize(new Dimension(120, 22));
				//panel.add(pfdPassword);

		// Levelauswahl
		//JLabel lblLevel = new JLabel("Level:");
		//lblLevel.setForeground(Color.orange);
		//pnlButtons.add(lblLevel);

		//Levelnamen auslesen
		arrLevel = this.alienDefenceController.getLevelController().readAllLevels();
		String[] arrLevelNames = getLevelNames(arrLevel);
		
		//Combobox erstellen
		setCboLevelChooser(new JComboBox(arrLevelNames));
		//pnlButtons.add(cboLevelChooser);
		
		//Spiel starten
		//Die Textfelder werden ausgewertet und das Passwort validiert, dann wird das Spiel gestartet
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.setPreferredSize(new Dimension(75, 50));
		btnSpielen.setFont(new Font("Microsoft JhengHei", Font.BOLD, 20));
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				type_id = 1;
				new LeveldesignWindow(alienDefenceController.getLevelController(), alienDefenceController.getTargetController(), alienDefenceController, type_id);
				//btnSpielen_Clicked(alienDefenceController, arrLevel);
			}
		});
		pnlButtons.add(btnSpielen);
		
		JButton btnTesten = new JButton("Testen");
		btnTesten.setPreferredSize(new Dimension(71, 50));
		btnTesten.setFont(new Font("Microsoft JhengHei", Font.BOLD, 20));
		btnTesten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				type_id = 2;
				new LeveldesignWindow(alienDefenceController.getLevelController(), alienDefenceController.getTargetController(), alienDefenceController, type_id);
			}
		});
		pnlButtons.add(btnTesten);
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.setPreferredSize(new Dimension(89, 50));
		btnHighscore.setFont(new Font("Microsoft JhengHei", Font.BOLD, 20));
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Highscore(alienDefenceController.getAttemptController(), getCboLevelChooser().getSelectedIndex());
			}
		});
		pnlButtons.add(btnHighscore);
		
		JButton btnLeveleditor = new JButton("Leveleditor");
		btnLeveleditor.setPreferredSize(new Dimension(95, 50));
		btnLeveleditor.setFont(new Font("Microsoft JhengHei", Font.BOLD, 20));
		btnLeveleditor.setBackground(Color.ORANGE);
		btnLeveleditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				type_id = 3;
				new LeveldesignWindow(alienDefenceController.getLevelController(), alienDefenceController.getTargetController(), alienDefenceController, type_id);
			}
		});
		pnlButtons.add(btnLeveleditor);
		
		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.setPreferredSize(new Dimension(81, 50));
		btnBeenden.setFont(new Font("Microsoft JhengHei", Font.BOLD, 20));
		btnBeenden.setBackground(Color.GRAY);
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		pnlButtons.add(btnBeenden);

		//Logo		
		JPanel pnlLogo = new JPanel();
		pnlLogo.setMaximumSize(new Dimension(30000, 30000));
		pnlLogo.setAutoscrolls(true);
		pnlLogo.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		pnlLogo.setBackground(Color.BLACK);
		contentPane.add(pnlLogo, BorderLayout.CENTER);

		JLabel lblLogo = new JLabel("");
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("./pictures/logo.png").getImage().getScaledInstance(460, 460, Image.SCALE_DEFAULT));
		lblLogo.setIcon(imageIcon);
		pnlLogo.add(lblLogo);
	}

	private String[] getLevelNames(List<Level> arrLevel) {
		String[] arrLevelNames = new String[arrLevel.size()];

		for (int i = 0; i < arrLevel.size(); i++) {
			arrLevelNames[i] = arrLevel.get(i).getName(); // Array aus Arraylist erstellt
		}

		return arrLevelNames;
	}
	
	public static JComboBox getCboLevelChooser() {
		return cboLevelChooser;
	}

	public static void setCboLevelChooser(JComboBox cboLevelChooser) {
		MainMenue.cboLevelChooser = cboLevelChooser;
	}
	
	public static void btnSpielen_Clicked(AlienDefenceController alienDefenceController, List<Level> arrLevel) {
		// User aus Datenbank holen
		//TODO B�ser Versto� gegen MVC - hier muss sp�ter nochmal nachgebessert werden
		User user = new PersistanceDB().getUserPersistance().readUser(tfdLogin.getText());

		// Spielstarten, wenn Nutzer existiert und Passwort �bereinstimmt
		if (user != null && user.getPassword().equals(new String(pfdPassword.getPassword()))) {

			Thread t = new Thread("GameThread") {
				@Override
				public void run() {

					GameController gameController = alienDefenceController.startGame(arrLevel.get(getCboLevelChooser().getSelectedIndex()), user);
					new GameGUI(gameController).start();
				}
			};
			t.start();
		} else {
			// Fehlermeldung - Zugangsdaten fehlerhaft
			JOptionPane.showMessageDialog(null, "Zugangsdaten nicht korrekt", "Fehler",
					JOptionPane.ERROR_MESSAGE);
		}
	}

}