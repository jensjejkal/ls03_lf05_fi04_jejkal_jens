package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import model.persistanceDB.PersistanceDB;
import view.game.GameGUI;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private List<Level> arrLevel;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, int type_id) {
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setForeground(Color.ORANGE);
		setBackground(Color.BLACK);
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		arrLevel = this.lvlControl.readAllLevels();
		
		
		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(new Color(0, 0, 0));
		add(pnlButtons, BorderLayout.SOUTH);
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setOpaque(true);
		lblLevelauswahl.setBackground(new Color(0, 0, 0));
		lblLevelauswahl.setFont(new Font("OCR A Extended", Font.BOLD, 50));
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setForeground(Color.ORANGE);
		spnLevels.setBackground(Color.BLACK);
		spnLevels.getViewport().setBackground(Color.BLACK);
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setFont(new Font("Microsoft JhengHei", Font.BOLD, 20));
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblLevels.setRowHeight(30);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();

		pnlButtons.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 5));
		
		if(type_id == 1) {
			JButton btnSpielen = new JButton("Spielen");
			btnSpielen.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
			btnSpielen.setBackground(Color.DARK_GRAY);
			btnSpielen.setForeground(Color.ORANGE);
			btnSpielen.setPreferredSize(new Dimension(200, 40));
			pnlButtons.add(btnSpielen);
			btnSpielen.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MainMenue.btnSpielen_Clicked(alienDefenceController, arrLevel);
				}
			});
		}
		
		if(type_id == 2) {
		JButton btnTesten = new JButton("Testen");
		btnTesten.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
		btnTesten.setBackground(Color.DARK_GRAY);
		btnTesten.setForeground(Color.ORANGE);
		btnTesten.setPreferredSize(new Dimension(200, 40));
		pnlButtons.add(btnTesten);
		btnTesten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				User user = new User(1, "test", "pass");

				Thread t = new Thread("GameThread") {

					//@Override
					public void run() {

						GameController gameController = alienDefenceController.startGame(arrLevel.get(0), user);
						new GameGUI(gameController).start();
					}
				};
				t.start();
			}
		});
		}
		
		if(type_id == 3) {
			
			JButton btnNewLevel = new JButton("Neues Level");
			btnNewLevel.setBackground(Color.DARK_GRAY);
			btnNewLevel.setForeground(Color.ORANGE);
			btnNewLevel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					btnNewLevel_Clicked();
				}
			});
			btnNewLevel.setPreferredSize(new Dimension(200,40));
			btnNewLevel.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
			pnlButtons.add(btnNewLevel);
		

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
		btnUpdateLevel.setBackground(Color.DARK_GRAY);
		btnUpdateLevel.setForeground(Color.ORANGE);
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
		btnDeleteLevel.setBackground(Color.DARK_GRAY);
		btnDeleteLevel.setForeground(Color.ORANGE);
		btnDeleteLevel.setPreferredSize(new Dimension(200,40));
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);
		
		}
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
		btnHighscore.setBackground(Color.DARK_GRAY);
		btnHighscore.setForeground(Color.ORANGE);
		btnHighscore.setPreferredSize(new Dimension(175,40));
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Highscore(alienDefenceController.getAttemptController(), MainMenue.getCboLevelChooser().getSelectedIndex());
			}
		});
		pnlButtons.add(btnHighscore);
		
		JButton btnBeenden = new JButton("Levelauswahl beenden");
		btnBeenden.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
		btnBeenden.setBackground(Color.DARK_GRAY);
		btnBeenden.setForeground(Color.ORANGE);
		btnBeenden.setPreferredSize(new Dimension(350, 40));
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leveldesignWindow.setVisible(false);
			}
		});
		pnlButtons.add(btnBeenden);
		
		JButton btnSchliessen = new JButton("Programm schlie�en");
		btnSchliessen.setFont(new Font("Maiandra GD", Font.PLAIN, 26));
		btnSchliessen.setBackground(Color.DARK_GRAY);
		btnSchliessen.setForeground(Color.ORANGE);
		btnSchliessen.setPreferredSize(new Dimension(325, 40));
		btnSchliessen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		pnlButtons.add(btnSchliessen);
		
	}
	

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
}
